
public class Tree {
    private double height;
    private boolean natural;
    private double balance;

    public Tree(double height, boolean natural, double balance) {
        this.height = height;
        this.natural = natural;
        this.balance = balance;
    }

    public double getHeight() {
        return height;
    }

    public boolean isNatural() {
        return natural;
    }

    public double getBalance() {
        return balance;
    }

    @Override
    public String toString() {
        return "Елка красивая,пушистая,бери-радоваться будешь :) \n" +
                "(Высота " + height +
                ", натуральная " + natural +
                ", цена великолепной елки - " + balance +
                ")";
    }
    /**
     *natur сравнивает натуральность Елки,которую хочет пользователь с Елками ,которые есть в наличии
     */
    boolean natur(Tree tree) {
        return this.natural == tree.isNatural();
    }

    /**
     *balanced сравнивает цену Елки,которую хочет пользователь с Елками ,которые есть в наличии
     */

    public boolean balanced(Tree tree) {
        boolean z = false;
        for (int i = 0; i < 100; i++) {
            boolean b = (this.balance + i == tree.balance);
            boolean a = (this.balance - i == tree.balance);
            if (b == true || a == true) {
                z = true;
            }
        }
        return z;
    }

    /**
     *hght сравнивает высоту Елки,которую хочет пользователь с Елками ,которые есть в наличии
     */

    boolean hght(Tree tree) {
        boolean z = false;
        for (double i = 0; i < 0.2; i = i + 0.1) {
            boolean b = (this.height + i == tree.height);
            boolean a = (this.height - i == tree.height);
            if (b == true || a == true) {
                z = true;
            }
        }
        return z;
    }
}
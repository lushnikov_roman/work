
import java.util.Scanner;
import java.util.ArrayList;

public class Main {
    private static Scanner scanner = new Scanner(System.in);
    /**
     * main создает объекты с характеристиками и направляет в методы
     */
    public static void main(String[] args) {
        Tree tree1 = new Tree(0.5, false, 450);
        Tree tree2 = new Tree(0.6, true, 500);
        Tree tree3 = new Tree(0.7, false, 550);
        Tree tree4 = new Tree(0.8, true, 600);
        Tree tree5 = new Tree(0.9, false, 650);
        Tree tree6 = new Tree(1.0, true, 650);
        Tree tree7 = new Tree(1.1, true, 700);
        Tree tree8 = new Tree(1.2, true, 750);
        Tree tree9 = new Tree(1.3, true, 800);
        Tree tree10 = new Tree(1.4, true, 850);
        Tree tree11 = new Tree(1.5, true, 900);
        Tree tree12 = new Tree(1.6, true, 950);
        Tree tree13 = new Tree(1.7, true, 1000);
        Tree tree14 = new Tree(1.8, true, 1050);
        Tree tree15 = new Tree(1.8, true, 1100);
        Tree tree16 = new Tree(1.9, true, 1150);
        Tree tree17 = new Tree(2, true, 1200);
        Tree tree18 = new Tree(2.1, true, 1250);
        Tree tree19 = new Tree(2.2, true, 1300);
        Tree tree20 = new Tree(2.3, true, 1350);
        Tree tree21 = new Tree(2.4, true, 1400);
        Tree tree22 = new Tree(2.5, true, 1450);
        Tree tree23 = new Tree(2.6, true, 1500);
        Tree tree24 = new Tree(2.7, true, 1550);
        Tree tree25 = new Tree(2.8, true, 1600);
        Tree tree26 = new Tree(2.9, true, 1650);
        Tree tree27 = new Tree(3, true, 1700);
        Tree tree28 = new Tree(0.5, false, 450);
        Tree tree29 = new Tree(0.6, false, 500);
        Tree tree30 = new Tree(0.7, false, 550);
        Tree tree31 = new Tree(0.8, false, 600);
        Tree tree32 = new Tree(0.9, false, 650);
        Tree tree33 = new Tree(1.0, false, 650);
        Tree tree34 = new Tree(1.1, false, 700);
        Tree tree35 = new Tree(1.2, false, 750);
        Tree tree36 = new Tree(1.3, false, 800);
        Tree tree37 = new Tree(1.4, false, 850);
        Tree tree38 = new Tree(1.5, false, 900);
        Tree tree39 = new Tree(1.6, false, 950);
        Tree tree40 = new Tree(1.7, false, 1000);
        Tree tree41 = new Tree(1.8, false, 1050);
        Tree tree42 = new Tree(1.8, false, 1100);
        Tree tree43 = new Tree(1.9, false, 1150);
        Tree tree44 = new Tree(2, false, 1200);
        Tree tree45 = new Tree(2.1, false, 1250);
        Tree tree46 = new Tree(2.2, false, 1300);
        Tree tree47 = new Tree(2.3, false, 1350);
        Tree tree48 = new Tree(2.4, false, 1400);
        Tree tree49 = new Tree(2.5, false, 1450);
        Tree tree50 = new Tree(2.6, false, 1500);
        Tree tree51 = new Tree(2.7, false, 1550);
        Tree tree52 = new Tree(2.8, false, 1600);
        Tree tree53 = new Tree(2.9, false, 1650);
        Tree tree54 = new Tree(3, true, 1700);
        ArrayList<Tree> trees = new ArrayList<>();
        trees.add(tree1);
        trees.add(tree2);
        trees.add(tree3);
        trees.add(tree4);
        trees.add(tree5);
        trees.add(tree6);
        trees.add(tree7);
        trees.add(tree8);
        trees.add(tree9);
        trees.add(tree10);
        trees.add(tree11);
        trees.add(tree12);
        trees.add(tree13);
        trees.add(tree14);
        trees.add(tree15);
        trees.add(tree16);
        trees.add(tree17);
        trees.add(tree18);
        trees.add(tree19);
        trees.add(tree20);
        trees.add(tree21);
        trees.add(tree22);
        trees.add(tree23);
        trees.add(tree24);
        trees.add(tree25);
        trees.add(tree26);
        trees.add(tree27);
        trees.add(tree28);
        trees.add(tree29);
        trees.add(tree30);
        trees.add(tree31);
        trees.add(tree32);
        trees.add(tree33);
        trees.add(tree34);
        trees.add(tree35);
        trees.add(tree36);
        trees.add(tree37);
        trees.add(tree38);
        trees.add(tree39);
        trees.add(tree40);
        trees.add(tree41);
        trees.add(tree42);
        trees.add(tree43);
        trees.add(tree44);
        trees.add(tree45);
        trees.add(tree46);
        trees.add(tree47);
        trees.add(tree48);
        trees.add(tree49);
        trees.add(tree50);
        trees.add(tree51);
        trees.add(tree52);
        trees.add(tree53);
        trees.add(tree54);
        trees.add(input());
        Eli(trees);
    }
    /**
     *input()  создает объект с характеристиками пользователя
     */
    private static Tree input() {
        System.out.print("Введите необходимую высоту (в метрах) - ");
        double height = scanner.nextDouble();
        System.out.print("Вам необходима натуральная ёлка?");
        boolean natural = scanner.nextBoolean();
        System.out.print("Введите стоимость ёлки - ");
        double balance = scanner.nextDouble();
        return new Tree(height, natural, balance);
    }

    /**
     *Eli(ArrayList<Tree> trees)  подбирает Елку под характеристики пользователя
     */

    private static void Eli(ArrayList<Tree> trees) {
        int treeSize = trees.size();
        for (int i = 0; i < (treeSize - 1); i++) {
            if (trees.get(i).natur(trees.get(54)) == true && trees.get(i).hght(trees.get(54)) == true && trees.get(i).balanced(trees.get(54)) == true) {
                System.out.println("Эта елка вам подходит:\n" + trees.get(i));
            }
        }
    }
}


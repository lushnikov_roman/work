package ru.lrs.temperature;

import java.util.Scanner;

public class Temperature {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int numberOfDays;
        System.out.print("Введите количество дней:");
        numberOfDays = scanner.nextInt();
        double temperature[] = new double [numberOfDays];
        fillTemperature(temperature);
        System.out.println("");
        System.out.printf("Avg temp:%d", averageTemperature(temperature));
        System.out.println("");
        double max = getMax(temperature);
        System.out.printf("Maximum temperature is: %5.1f ", max);
        System.out.println("");
        double min = getMin(temperature);
        System.out.printf("Minimum temperature is: %5.1f ", min);
        System.out.println("");
        сoldday(temperature,min);
        warmday(temperature,max);
    }

    /**
     *coldday выводит информацию о холодных днях
     */

    private static void сoldday(double[] temperature,double min) {
        int number[] = new int[30];
        int x = 0;
        for (int i = 0; i < temperature.length; i++) {
            if (min == temperature[i]) {
                number[x] = i + 1;
                System.out.printf("Cold day: %d   ",number[x]);
                x++;
            }
        }
    }

    /**
     *warmday выводит информацию о теплых днях
     */

    private static void warmday(double[] temperature,double max) {
        int number[] = new int[30];
        int x = 0;
        for (int i = 0; i < temperature.length; i++) {
            if (max == temperature[i]) {
                number[x] = i + 1;
                System.out.printf("   Warm day:%d   ",number[x]);
                x++;
            }
        }
    }

    /**
     *fillTemperature заполняет массив рандомной температурой и сразу же ее выводит
     */

    private static void fillTemperature(double[] temperature) {
        System.out.println("days temperature: ");
        for (int i = 0; i < temperature.length; i++) {
            temperature[i] = (-20 + (Math.random() * 30));
            System.out.printf("%5.1f", temperature[i]);
            System.out.println(" ");
        }
    }

    /**
     *getMin ищет минимальную температуру всех дней
     */

    private static double getMin(double temperature[]) {
        double minValue = temperature[0];
        for (int i = 1; i < temperature.length; i++) {
            if (temperature[i] < minValue) {
                minValue = temperature[i];
            }
        }
        return minValue;
    }

    /**
     *getMax ищет максимальную температуру всех дней
     */

    private static double getMax(double temperature[]) {
        double maxValue = temperature[0];
        for (int i = 1; i < temperature.length; i++) {
            if (temperature[i] > maxValue) {
                maxValue = temperature[i];
            }
        }
        return maxValue;
    }

    /**
     *averageTemperature ищет среднюю температуру всех дней
     */

    private static int averageTemperature(double temperature[]) {
        double sum =0.0;
        for (double temperatureOfDay : temperature) {
            sum += temperatureOfDay;
        }
        return (int) (sum / temperature.length);
    }
}
package ru.lrs.seaBatlle;

import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int leighGateway = (int) (5 + Math.random() * 15);
        int shipLength = (int) (1 + Math.random() * 4);
        int start = (int) (1 + Math.random() * leighGateway - shipLength);
        System.out.println("Длинна шлюза - " + (leighGateway));
        Ship ship = new Ship(start, shipLength);
        shots(ship);
    }

    /**
     * shots спрашивает пользователя точку выстрела,а после сравнивает с нахождением кораблся и выводит информацию о выстреле
     */

    private static void shots(Ship ship) {
        int x = 0;
        int shot;
        do {
            System.out.println("Куда стреляем,капитан?");
            shot = scanner.nextInt();
            System.out.println(ship.shot(shot));
            x++;
        } while (!ship.shot(shot).equals("Корабль убит,экипаж контужен!"));
        System.out.println("Вы потопили корабль с " + x + " попытки!");
    }
}

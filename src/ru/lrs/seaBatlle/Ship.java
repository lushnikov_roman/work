package ru.lrs.seaBatlle;
import java.util.ArrayList;

class Ship {
    private ArrayList<Integer> location = new ArrayList<>();

    Ship(int start, int shipLength) {
        for (int i = start; i < start + shipLength; i++) {
            location.add(i);
        }
    }

    String shot(int shot) {
        String message = "Мимо";
        if (location.contains(shot)) {
            location.remove((Integer) shot);
            message = "Пробитие";
        }
        if (location.isEmpty()) {
            message = "Корабль убит,экипаж контужен!";
        }
        return message;
    }

    @Override
    public String toString() {
        return "Корабль (" + location + ')';
    }

}

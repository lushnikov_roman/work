package ru.lrs.baggage;

import java.util.ArrayList;

import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);

    /**
     * В классе main узнаем кол-во пассажиров
     * ArrayList - Создали массив и положили в него пассажиров
     * input()- это метод создающий объекты типа "Baggage"
     * pasClad - Выводит информацию о пассажире с ручной кладью
     */

    public static void main(String[] args) {
        System.out.print("Сколько пассажиров вы хотите ввести: ");
        int num = scanner.nextInt();
        ArrayList<Baggage> baggages = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            baggages.add(input());
        }
        pasClad(baggages);
    }

    private static Baggage input() {
        System.out.print("Введите фамилию: ");
        scanner.nextLine();
        String surname = scanner.nextLine();
        System.out.println("Введите количество багажных мест: ");
        int number = scanner.nextInt();
        System.out.print("Общий вес багажа : ");
        double weight = scanner.nextDouble();
        return new Baggage(surname, number, weight);
    }

    private static void pasClad(ArrayList<Baggage> baggages) {
        int baggagesSize = baggages.size();
        for (int i = 0; i < baggagesSize; i++) {
            if (baggages.get(i).Сlad()) {
                System.out.println(baggages.get(i).getSurname() + " с ручной кладью");
            }
        }
    }
}
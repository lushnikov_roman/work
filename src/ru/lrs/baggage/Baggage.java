package ru.lrs.baggage;

public class Baggage {
    private String surname;
    private int number;
    private double weight;


    public Baggage(String surname, int number, double weight) {
        this.surname = surname;
        this.number = number;
        this.weight = weight;
    }

    public String getSurname() {
        return surname;
    }

    public int getNumber() {
        return number;
    }

    public double getWeight() {
        return weight;
    }

    boolean Сlad() {
        return number==1 && weight <= 10;
    }

    @Override
    public String toString() {
        return "Baggage{" +
                "surname='" + surname + '\'' +
                ", number=" + number +
                ", weight=" + weight +
                '}';
    }
}

package ru.lrs.college;
import java.util.Scanner;
import java.util.ArrayList;

public class Main {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        Student student1 = new Student(2018, "09.05.07", "Леонтьев", Gender.MAN);
        Student student2 = new Student(2017, "09.08.17", "Семенова", Gender.WOMAN);
        Student student3 = new Student(2016, "09.02.27", "Гришин", Gender.MAN);
        Student student4 = new Student(2014, "10.09.07", "Мешкова", Gender.WOMAN);
        Student student5 = new Student(2012, "15.06.00", "Павлов", Gender.MAN);

        ArrayList<Student> students = new ArrayList<>();

        students.add(student1);
        students.add(student2);
        students.add(student3);
        students.add(student4);
        students.add(student5);

        Teacher teacher1 = new Teacher("Русский", true, "Адельшина", Gender.WOMAN);
        Teacher teacher2 = new Teacher("Физ-ра", false, "Соломинский", Gender.MAN);
        Teacher teacher3 = new Teacher("Физика", true, "Дорофеев", Gender.MAN);
        Teacher teacher4 = new Teacher("История", false, "Кудряшева", Gender.WOMAN);
        Teacher teacher5 = new Teacher("ОС", true, "Николаева", Gender.WOMAN);

        ArrayList<Teacher> teachers = new ArrayList<>();


        teachers.add(teacher1);
        teachers.add(teacher2);
        teachers.add(teacher3);
        teachers.add(teacher4);
        teachers.add(teacher5);

        ArrayList<Person> personArrayList = new ArrayList<>();

        input0(teachers,students,personArrayList);
        System.out.println("Кол-во девушек, поступивших в 2017: " + studentInformation(students));
        teacherInformation(teachers);
        man(personArrayList);
    }

    /**
     * input0() - метод который узнает у пользователя кол-во человек которых хочет ввести пользователь и
     * узнает тип предмета(учитель или ученик),а затем направляет в другой метод для дальнейшей инициализации предмета,
     * добавляя новый или новые объекты в массив
     */

    private static ArrayList<Person> input0(ArrayList<Teacher> teachers, ArrayList<Student> students,ArrayList<Person> personArrayList) {
        System.out.println("Cколько человек вы хотите ввести?");
        int num = scanner.nextInt();
        for (int i = 0; i < num; i++) {
            System.out.println("Кого вы хотите занести в список?");
            if (scanner.nextLine().equals("ученик")) {
                students.add(input1());
            }
            if (scanner.nextLine().equals("учитель")) {
                teachers.add(input2());
            }
        }
        personArrayList.addAll(students);
        personArrayList.addAll(teachers);
        return personArrayList;
    }

    /**
     *input1()- метод который создает предметы типа "студент"
     */

    private static Student input1() {
        Gender gender = Gender.MAN;
        System.out.print("Год поступления: ");
        int ydull = scanner.nextInt();
        System.out.print("Введите специальность: ");
        scanner.nextLine();
        String specialty = scanner.nextLine();
        System.out.print("Введите фамилию: ");
        String surname = scanner.nextLine();
        System.out.print("Введите пол: ");
        String gend = scanner.nextLine();
        if (gend.equals("мужской")) {
            gender = Gender.MAN;
        }
        if (gend.equals("женский")) {
            gender = Gender.WOMAN;
        }
        return new Student(ydull, specialty, surname, gender);
    }

    /**
     *input2()- метод который создает предметы типа "учитель"
     */

    private static Teacher input2() {
        Gender gender = Gender.MAN;
        System.out.print("Дисциплина учителя: ");
        String discipline = scanner.nextLine();
        System.out.print("Куратор?: ");
        boolean curator = scanner.nextBoolean();
        System.out.print("Введите фамилию: ");
        scanner.nextLine();
        String surname = scanner.nextLine();
        System.out.print("Введите пол: ");
        String gend = scanner.nextLine();
        if (gend.equals("мужской")) {
            gender = Gender.MAN;
        }
        if (gend.equals("женский")) {
            gender = Gender.WOMAN;
        }
        return new Teacher(discipline, curator, surname, gender);
    }

    /**
     *studentInformation- метод который находит кол-во девочек 2017 года поступления
     */

    private static int studentInformation(ArrayList<Student> students) {
        int x = 0;
        for (Student student : students) {
            if (student.getYdull() == 2017 && student.getGender() == Gender.WOMAN) {
                x = x + 1;
            }
        }
        return x;
    }

    /**
     *teacherInformation- метод который находит учителей,которые кураторы и выводит о них информацию
     */

    private static void teacherInformation(ArrayList<Teacher> teachers) {
        for (Teacher teacher : teachers) {
            if (teacher.isCurator() == true) {
                System.out.println(teacher.getSurname() + " является куратором");
            }
        }
    }

    /**
     *man- метод который находит персон мужского пола
     */

    private static void man(ArrayList<Person> personArrayList) {
        for (Person person : personArrayList) {
            if (person.getGender() == Gender.MAN) {
                System.out.println(person.getSurname() + " настоящий мужик!");
            }
        }
    }
}
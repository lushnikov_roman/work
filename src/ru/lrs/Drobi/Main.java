package ru.lrs.Drobi;

import java.util.ArrayList;
import java.util.Scanner;

abstract class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите выражение: ");
        String expression = scanner.nextLine();
        ArrayList<Rational> rationals = new ArrayList<>();
        input(expression, rationals);
        operation(expression, rationals);
    }

    /**
     * Метод ArrayList<Rational> input для ввода двух объектов(дробей): числителя и знаменателя 1 дроби, числителя и знаменателя 2 дроби
     * return возвращает массив дробей
     *
     * @param expression строка выражения
     * @param rationals массив дробей
     * @return
     */
    private static ArrayList<Rational> input(String expression, ArrayList<Rational> rationals) {
        String[] array = expression.split(" ");
        String[] number1 = String.valueOf(array[0]).split("/");
        int num1 = Integer.parseInt(number1[0]);
        int den1 = Integer.parseInt(number1[1]);
        if (den1 == 0 ) {
            System.out.println("Дробь некорректна!!!");
        }else {
            rationals.add(new Rational(num1, den1));
        }
        String[] number2 = String.valueOf(array[2]).split("/");
        int num2 = Integer.parseInt(number2[0]);
        int den2 = Integer.parseInt(number2[1]);
        if (den2 == 0 ) {
            System.out.println("Дробь некорректна!!!");
        }else {
            rationals.add(new Rational(num2, den2)); }
        return rationals;

    }
    /**
     * Метод operation определяет знак, введеный пользователем знак и с помощью оператора switch обрабатывает операцию
     *
     * @param expression строка выражения
     * @param rationals массив дробей
     */

    private static void operation(String expression, ArrayList<Rational> rationals) {
        String[] array = expression.split(" ");
        String exp = array[1];
        switch (exp) {
            case "+":
                Rational rational1 = rationals.get(0).add(rationals.get(1));
                System.out.println("Ответ: \n" + rationals.get(0) + " " + exp + " " + rationals.get(1) + " = " + rational1);
                break;

            case "-":
                Rational rational2 = rationals.get(0).sub(rationals.get(1));
                System.out.println("Ответ: \n" + rationals.get(0) + " " + exp + " " + rationals.get(1) + " = " + rational2);
                break;

            case "/":
                Rational rational3 = rationals.get(0).div(rationals.get(1));
                System.out.println("Ответ: \n" + rationals.get(0) + " " + exp + " " + rationals.get(1) + " = " + rational3);
                break;

            case "*":
                Rational rational4 = rationals.get(0).mult(rationals.get(1));
                System.out.println("Ответ: \n" + rationals.get(0) + " " + exp + " " + rationals.get(1) + " = " + rational4);
                break;

            default:
                System.out.println("Введен не правильный знак операции!!!");

        }
    }
}
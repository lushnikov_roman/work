package ru.lrs.country;

public class Country {
    private String name;
    private String capital;
    private double area;
    private int population;


    public Country(String name, String capital, double area, int population) {
        this.name = name;
        this.capital = capital;
        this.area = area;
        this.population = population;

    }

    public Country(){
        this("Неизвестно","Неизвестно",0,0);
    }


    public Double bigPopulation(){
        double density;
        density=population/area;
        return density;
    }

    @Override
    public String toString() {
        return "Country{" +
                "name='" + name + '\'' +
                ", capital='" + capital + '\'' +
                ", area=" + area +
                ", population=" + population +
                '}';
    }
}
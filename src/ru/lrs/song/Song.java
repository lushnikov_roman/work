package ru.lrs.song;

public class Song {

    private String name;
    private String executor;
    private int duration;

    Song(String name, String executor, int duration) {
        this.name = name;
        this.executor = executor;
        this.duration = duration;
    }


    Song() {
        this("Не указано", "Не указано", 0);
    }

    public String getName() {
        return name;
    }

    public String getExecutor() {
        return executor;
    }

    public int getDuration() {
        return duration;
    }

    public String category() {
        String category = new String();
        if (duration < 120 ) {
            category = "short";
        }
        if (duration > 240 ) {
            category = "long";
        }
        if( duration >120 && duration < 240 ){
            category = "medium";
        }
        return category;
    }

    boolean isSameCategory(Song song){
        return this.category().equals(song.category());
    }

    @Override
    public String toString() {
        return "Song{" +
                "name='" + name + '\'' +
                ", executor='" + executor + '\'' +
                ", duration=" + duration +
                '}';
    }
}
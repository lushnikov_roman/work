package ru.lrs.box;

public class Box {

    private String name;
    private int width;
    private int height;
    private int depth;

    public Box(String name, int width, int height, int depth) {
        this.name = name;
        this.width = width;
        this.height = height;
        this.depth = depth;
    }

    public Box(String name, int side) {
        this(name, side, side, side);
    }

    public Box() {
        this("коробочка", 1);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getDepth() {
        return depth;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    @Override
    public String toString() {
        return name +
                " width=" + width +
                ", height=" + height +
                ", depth=" + depth +
                '}';
    }

    public int volume(int massiv[]) {
        width = Integer.parseInt(String.valueOf(width));
        height = Integer.parseInt(String.valueOf(height));
        depth = Integer.parseInt(String.valueOf(depth));
        int v = width * height * depth;
        for (int i = 1; i < massiv.length; i++) {
            v = massiv[i];
        }
        int maxV = massiv[1];
        for (int i = 1; i < massiv.length; i++)
            if (massiv[i] > maxV) {
                maxV = massiv[i];
            }
        return maxV;
    }

    }






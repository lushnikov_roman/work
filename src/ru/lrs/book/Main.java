package ru.lrs.book;
import java.util.Scanner;
import java.util.ArrayList;


public class Main {
    private static Scanner scanner = new Scanner(System.in);

    /**
     * В классе main создаём пять книг с исходными данными
     * ArrayList - Создали массив и положили в него книги которые инициализировали
     * printMessageBooks - выводит информацию сравниваемых книгах
     * printComparison - Выводит информацию о книге если она 2018 - го года издания
     */

    public static void main(String[] args) {
        Book book1 = new Book("Пушкин", " Евгений Онегин", 2018);
        Book book2 = new Book("Фет", "Огонь", 2001);
        Book book3 = new Book("Гоголь", "Поле", 2003);
        Book book4 = new Book("Лермонтов", "Тарханы", 2017);
        Book book5 = new Book("Белинский", "О пензе", 2018);

        ArrayList<Book> books = new ArrayList<>();
        books.add(book1);
        books.add(book2);
        books.add(book3);
        books.add(book4);
        books.add(book5);
        books.add(input());
        printMessageBooks(books);
        printComparison(books);
    }

    /**
     *
     * input - Запрашивает у пользователя информацию для сравнения в Main
     *
     * @return - Возвращаем новому объекту book параметры
     */

    private static Book input() {
        System.out.println("Введите автора : ");
        String surname = scanner.nextLine();
        System.out.println("Введите название книги : ");
        String name = scanner.nextLine();
        System.out.println("Введите год издания книги : ");
        int year = scanner.nextInt();
        return new Book(surname, name, year);
    }

    /**
     * printMessageBooks - Выводит информацию, равен ли год издания сравниваемых книг
     *
     * @param books - Используем масив книг
     */

    private static void printMessageBooks(ArrayList<Book> books) {
        int booksSize = books.size();
        if (books.get(0).isYears(books.get(booksSize - 1))) {
            System.out.println("Год издания книги - " + books.get(0).getName() + "- и книги - " + books.get(booksSize - 1).getName() + " - равны ");
        } else {
            System.out.println("Год издания книги - " + books.get(0).getName() + "- и книги - " + books.get(booksSize - 1).getName() + " - не равны");
        }
    }

    /**
     * printComparison - приравнивает сравниваемые книги с 2018-м годом и выводим информацию если год 2018.
     *
     * @param books - Использую массив книг.
     */

    private static void printComparison(ArrayList<Book> books) {
        for (Book book : books) {
            if (book.getYear() == 2018) {
                System.out.println("Книга - " + book.getName() + " - 2018-го года издания \n" + book);

            }
        }
    }
}
package ru.lrs.shape;

public class Triangle extends Shape {
    private Point a;
    private Point b;
    private Point c;

    public Triangle (Color color, Point a, Point b, Point c){
        super(color);
        this.a=a;
        this.b=b;
        this.c=c;
    }

    @Override
    public String toString() {
        return "Triangle{" +
                "a=" + a +
                ", b=" + b +
                ", c=" + c +
                '}';
    }

    @Override
    public double area() {
        return 0.5*Math.abs((b.getX()-a.getX())*(c.getY()-a.getY())-(c.getX()-a.getX())*(b.getY()-a.getY()));
    }
}

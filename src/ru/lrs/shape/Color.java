package ru.lrs.shape;

public enum Color {
    RED,
    WHITE,
    BLACK,
    BLUE
}

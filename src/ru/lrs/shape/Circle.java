package ru.lrs.shape;

public class Circle extends Shape {
    private Point center;
    private double radius;

    Circle(Color color, Point center, double radius) {
        super(color);
        this.center = center;
        this.radius = radius;
    }


    @Override
    public double area() {

        return Math.PI * radius * radius;
    }


    @Override
    public String toString() {
        return "круг{" +
                "цвет=" +
                "центр=" + center +
                ", радиус=" + radius +
                '}';
    }
}
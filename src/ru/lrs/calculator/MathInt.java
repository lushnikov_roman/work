package ru.lrs.calculator;

public class MathInt {

    /**
     *add - метод сложения
     */

    public static int add(int number1,int number2) {
        int result = number1 + number2 ;
        return result;
    }

    /**
     *sub - метод вычитания
     */

    public static int sub(int number1,int number2) {
        int result = number1 - number2 ;
        return result;
    }

    /**
     *div - метод деления
     */

    public static int div(int number1,int number2) {
        int result = number1 / number2 ;
        return result;
    }

    /**
     *mult - метод умножения
     */

    public static int mult(int number1,int number2) {
        int result = number1 * number2 ;
        return result;
    }

    /**
     *exp - метод возведения в степень
     */

    public static int exp(int number1, int number2) {
        int result = 1;
        for (int i = 0; i < number2; i++) {
            result *= number1;
        }
        return result;
    }

    /**
     *rem - метод выявления остатка после деления
     */

    public static int rem(int number1,int number2) {
        int result =number1 % number2 ;
        return result;
    }
}

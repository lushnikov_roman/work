package ru.lrs.robot;

import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Robot robot = input();
        System.out.println(robot);
        System.out.print("Введите конечную позицию x:");
        int endX = scanner.nextInt();
        System.out.print("Введите конечную позицию y:");
        int endY = scanner.nextInt();

        robot.move(endX, endY);
        System.out.println(robot);
    }

    /**
     * input() создает объекты с характеристиками которые укажет пользователь
     */


    private static Robot input() {
        Direction directionNow = Direction.UP;
        System.out.print("Введите начальную позицию x:");
        int x = scanner.nextInt();
        System.out.print("Введите начальную позицию y:");
        int y = scanner.nextInt();
        System.out.print("Введите куда изначально смотрит(при желании):");
        scanner.nextLine();
        String lookAt = scanner.nextLine();
        switch (lookAt) {
            case "вверх":
                directionNow = Direction.UP;
                break;
            case "вниз":
                directionNow = Direction.DOWN;
                break;
            case "вправо":
                directionNow = Direction.RIGHT;
                break;
            case "влево":
                directionNow = Direction.LEFT;
                break;
            default:
                break;
        }
        return new Robot(x, y, directionNow);
    }

}


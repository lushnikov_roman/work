package ru.lrs.robot;


public class Robot {
    private int x;
    private int y;
    private Direction direction;

    public Robot (int x, int y, Direction direction ){
        this.x = x;
        this.y = y;
        this.direction = direction;
    }

    public Robot (int x, int y){
        this(x,y,Direction.UP);
    }

    public Robot (){
        this(0,0,Direction.UP);
    }

    public void turnRight (){
        switch (direction){
            case UP:
                direction = Direction.RIGHT;
                break;
            case RIGHT:
                direction = Direction.DOWN;
                break;
            case DOWN:
                direction = Direction.LEFT;
                break;
            case LEFT:
                direction = Direction.UP;
                break;
            default:
                break;
        }
    }

    public void oneStep() {

        switch (direction){
            case UP:
                y++;
                break;
            case RIGHT:
                x++;
                break;
            case DOWN:
                y--;
                break;
            case LEFT:
                x--;
                break;
            default:
                break;
        }
    }

    public void move(int endX, int endY){
        if (endX>x) {
            while (direction!=Direction.RIGHT) {
                turnRight();
            }
            while (x!=endX) {
                oneStep();
            }
        }
        if (endX<x) {
            while (direction!=Direction.LEFT) {
                turnRight();
            }
            while (x!=endX) {
                oneStep();
            }
        }
        if (endY>y) {
            while (direction!=Direction.UP) {
                turnRight();
            }
            while (y!=endY) {
                oneStep();
            }
        }
        if (endY<y) {
            while (direction!=Direction.DOWN) {
                turnRight();
            }
            while (y!=endY) {
                oneStep();
            }
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Direction getWay() {
        return direction;
    }

    @Override
    public String toString() {
        return "Robot{" +
                "x=" + x +
                ", y=" + y +
                ", direction=" + direction +
                '}';
    }
}
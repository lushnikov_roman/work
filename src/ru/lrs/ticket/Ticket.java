package ru.lrs.ticket;

import java.util.Scanner;

public class Ticket {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите номер вашего билета :");
        int t = sc.nextInt();
        if (t < 1001) {
            System.out.println("Введите корректный номер!");
            return;
        }
        if (t > 1000000) {
            System.out.println("Введите корректный номер!");
            return;
        }
        int x6 = t / 100000;
        int x5 = t % 100000 / 10000;
        int x4 = t % 10000 / 1000;
        int x3 = t % 1000 / 100;
        int x2 = t % 100 / 10;
        int x1 = t % 10;
        if ((x1 + x2 + x3) == (x4 + x5 + x6)) {
            System.out.println("Поздравляю!Ваш билет счастливый!");
        } else {
            System.out.println("Ваш билет несчастливый...в другой раз повезет! :) ");
        }
    }
}
package ru.lrs.bank;

public class Bank {
    private double balance;
    private double percent;

    public Bank(double balance, double percent) {
        this.balance = balance;
        this.percent = percent;
    }

    public double getBalance() {
        return balance;
    }

    public double getPercent() {
        return percent;
    }

    public double podschet(){
     double p1=balance/100;
     double newBalance=balance+(p1*percent);
    return newBalance;
    }
}

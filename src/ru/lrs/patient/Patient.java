package ru.lrs.patient;

public class Patient {
    private String surname;
    private int year;
    private int number;
    private boolean dispensary;

    public Patient(String surname, int year, int number, boolean dispensary) {
        this.surname = surname;
        this.year = year;
        this.number = number;
        this.dispensary = dispensary;
    }


    public String getSurname() {
        return surname;
    }

    public int getYear() {
        return year;
    }

    public int getNumber() {
        return number;
    }

    public boolean getDispensary() {
        return dispensary;
    }

    public Patient(){
        this("не указано",0, 0, false);
    }

    @Override
    public String toString() {
        return "Patient{" +
                "Surname='" + surname + '\'' +
                ", Year=" + year +
                ", number=" + number +
                ", Dispensary=" + dispensary +
                '}';
    }


}